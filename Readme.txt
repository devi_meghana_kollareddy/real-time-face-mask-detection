PROJECT TITLE: REAL TIME FACE MASK DETECTION
Steps to execute the files:
Step – 1	: Download the zipped project file
Step – 2	: Unzip the project file
Step – 3	: Install anaconda prompt
Step – 4	: Create a file named train_mask_detector.py
		and paste the code from the file we have sent.
Step – 5		: In the DIRECTORY, we have to paste the path of dataset which we have downloaded by unzipping the file           
Step – 6	: Execute the file.
Step – 7	: We will get a graph naming “plot” and a model naming “mask_detector. model” in the disk.

Till here we have completed training module.
Now designing face mask detection.
Step – 8	: Copy the two files in the face detector file and paste it in disk.
Step - 9 	: Copy the code of detect_mask.py 
Step – 10	: Create a folder named detect_mask.py and paste the code.
Step – 11	     : Execute the code.
Step – 12	     : When we press ‘q’ the frame will be exit.
